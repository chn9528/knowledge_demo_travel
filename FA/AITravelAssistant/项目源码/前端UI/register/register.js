import fetch from '@system.fetch';
import prompt from '@system.prompt';
import router from '@system.router';


export default {
    data: {
        gender: "点击选择性别",
        genderVal: "",
        genders: ['男', '女'],

        occupation: "点击选择职业",
        occupationVal: "",
        occupations: ['保密','学者','艺术家','行政工作','大学生','导购','医生','高管','农民','家庭主妇','青少年','律师','程序猿','已退休','市场营销','科学家','自主创业','工程师','手艺人','待业','作家'],

        genre: "点击选择喜爱类型",
        genreVal: "",
        genres: ['休闲娱乐', '城市观光','主题乐园','亲子乐园','文化古迹','自然风光','展览展馆','名胜古迹','游山玩水','水上乐园','宗教风光','考古遗迹','地貌风景','远足','购物中心','动物植物','节庆活动','古村古镇'],

        region: "点击选择所在区域",
        regionVal: "",
        regions: ['南方', '北方'],

        birthday: "点击选择年龄段",
        birthdayVal: "",
        birthdays:['18岁以下','18-24','25-34','35-44','45-49','50-55','56+'],

    },
    ////////////性别选择器////////////
    chooseGender(e) {
        this.gender = e.newValue;
        this.genderVal = e.newSelected;
    },
    ////////////工作选择器////////////
    chooseOccupation(e) {
        this.occupation = e.newValue;
        this.occupationVal = e.newSelected;
    },
    ////////////喜好选择器////////////
    chooseGenre(e) {
        this.genre = e.newValue;
        this.genreVal = e.newSelected;
    },
    ////////////所在区域选择器////////////
    chooseRegion(e) {
        this.region = e.newValue;
        this.regionVal = e.newSelected;
    },
    ////////////年龄段选择器////////////
    chooseBirthday(e) {
        this.birthday = e.newValue;
        this.birthdayVal = e.newSelected;
    },
    ////////////昵称输入框////////////
    inputNickname(e) {
        this.nickname = e.value;
    },
    ////////////账号输入框////////////
    inputPhone(e) {
        this.uid = e.value;
    },
    ////////////密码输入框////////////
    inputPwd(e) {
        this.pwd = e.value;
    },
    ////////////确认密码输入框////////////
    inputchPwd(e) {
        this.chpwd = e.value;
    },

    ////////////点击注册时进行注册信息确认////////////
    register() {
        //当两次密码输入不一致时，提示消息
        if (this.chpwd != this.pwd) {
            prompt.showToast({
                message: "两次密码输入不一致！",
                duration: 3000
            });
        }
        //密码确认一致时，将信息传送至后端进行注册
        else {
            fetch.fetch({
                url: 'http://122.9.129.166:1918/register/',
                method: "POST",
                data: {
                    uid: this.uid,
                    pwd: this.pwd,
                    gender: this.genderVal,
                    age: this.birthdayVal,
                    nickname: this.nickname,
                    occupation: this.occupationVal,
                    region: this.regionVal,
                    genre: this.genreVal,
                },
                responseType: "json",
                //返回注册成功/注册失败消息
                success: res => {
                    let data = JSON.parse(res.data);
                    prompt.showToast({
                        message: data["注册信息"],
                        duration: 3000
                    });
                    //注册成功时，跳转到登陆界面
                    if (data["注册信息"] == "注册成功") {
                        router.push({
                            uri: "pages/login/login",
                            params: {
                                uid: this.uid,
                                pwd: this.pwd
                            } //params传输json格式数据给下一个界面，其他数据都会随着该界面的销毁而消失
                        });
                    }
                },
                //未正常从后端获取消息时展示
                fail: (resp) => {
                    prompt.showToast({
                        message: "获取数据失败",
                        duration: 3000
                    });
                    console.log("获取数据失败")
                }
            })
        }
    }
}
