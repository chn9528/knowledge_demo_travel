import router from '@system.router';
import fetch from '@system.fetch';
import prompt from '@system.prompt';

export default {
    ////////////初始化数据////////////
    data: {
        index : 0,
        info: {},
        list: [],
        but : "点击查看",
        select: [],
        numl : 0,
        numn : 0,
    },

    ////////////页面初始化，跳转页面时自动触发////////////
    onInit() {
    },
    onReady(){
    },

    ////////////点击按钮触发函数////////////
    onclick: function(e){
        //当按钮为点击查看时 点击按钮将所选择的城市、天数、紧凑程度以及个人信息中的个人喜好传入后端进行计算
        if (this.but == "点击查看")
        {
            var info = "";
            fetch.fetch({
                url: 'http://122.9.129.166:1918/run/',
                method: "POST",
                data: {
                    occupation: this.occupation,
                    age: this.age,
                    gender: this.gender,
                    region: this.region,
                    genre: this.genre,
                    time : this.time,
                    busy : this.busy,
                    city : this.city,
                },

                //成功连接并从后端获取到数据时
                success: (resp) => {
                    //令获取到的数据赋给info
                    info = resp.data;
                    //弹窗提示成功信息
                    prompt.showToast({
                        message: "数据加载中...",
                        duration: 3000
                    });
                    //console.log(info)

                    //去引号并且切分各个景点
                    info = info.slice(1, -2)
                    this.kaka = info.split(',')

                    for (var i = 0; i < this.kaka.length; i++) {
                        //解码转换成中文
                        this.kaka[i] = unescape(this.kaka[i].replace(/\\u/gi,'%u'))
                        //分别创建项目对象，可跳转至细节页面
                        var item = {
                            uri: "pages/detail/detail",
                            title: this.kaka[i],
                            id: i
                        }
                        this.index = this.index + 1;
                        this.list.push(item); //将新创建的item对象添加到list列表中
                    }
                    //所建议选择的景点数
                    this.numl = this.index - 4;
                    this.numn = this.index - 2;
                    //转变按钮形态
                    this.but = "确定";
                },
                //如果获取数据失败则执行以下函数
                fail: (resp) => {
                    prompt.showToast({
                        message: "获取数据失败",
                        duration: 3000
                    });
                    //console.log("获取数据失败")
                }
            });
        }
        //当按钮为确定时 点击按钮将所选择的景点以及个人信息中的个人喜好传入展示方案页面
        else{
            console.log("发送"+this.select)
            router.push({
                uri: "pages/show/show",
                params: {
                    occupation: this.occupation,
                    age: this.age,
                    gender: this.gender,
                    region: this.region,
                    genre: this.genre,
                    select : this.select,
                }//params传输json格式数据给下一个界面，其他数据都会随着该界面的销毁而消失
            });
        }
    },

    ////////////多选框结果记录函数////////////
    addChoice(e){
        if(e.checked){
            this.select.push(e.target.attr.value)
            //console.log("已选择"+this.select)
        }
        else{
            //当选框从已选择重新变为未选择时，从列表中删去选项
            var a = this.select.findIndex(e.target.arr.value)
            this.select.splice(a,1)
        }
    },

    ////////////点击按钮触发页面跳转函数，跳转至细节页面////////////
    jumpPage(id, uri) {
        router.push({
            uri: uri,
            params: {
                interest: this.kaka[id],
            }//params传输json格式数据给下一个界面，其他数据都会随着该界面的销毁而消失
        });
    }

}