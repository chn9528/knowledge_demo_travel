import prompt from '@system.prompt';
import router from '@system.router'
import fetch from '@system.fetch';

export default {
    data: {
        detail_title:"景点详细介绍",
        hidden: "hidden",
        title: "",
        num: 0,
        isState: true,
        isHidden: false,
        but : "点击查看",
        picture :"",
    },

    ////////////页面初始化，跳转页面时自动触发////////////
    onInit() {
        this.num = this.ref;
    },

    ////////////展示或隐藏下方的工具栏////////////
    show() {
        if (this.isHidden) {
            this.hidden = "hidden";
            this.isHidden = false
        } else {
            this.hidden = "visible";
            this.isHidden = true
        }
    },

    ////////////返回上一页面////////////
    jumpBack() {
        router.push({
            uri: "pages/choo/choo"
        })
    },
    ////////////跳转至上一页/下一页////////////
    change(choose) {
        if (this.num == 1 && choose == "previous") {
            prompt.showToast({
                message: this.$t('strings.first')
            })
        } else if (choose == "previous") {
            this.flash(--this.num)
        } else if (this.num == 15 && choose == "next") {
            prompt.showToast({
                message: this.$t('strings.last')
            })
        } else if (choose == "next") {
            this.flash(++this.num)
        }
    },

    ////////////点击按钮时触发////////////
    onclick: function(e){
        //按钮为点击查看时，向后端发送景点名，传回景点具体信息以及图片链接
        if (this.but == "点击查看")
        {
            var info = "";
            fetch.fetch({
                url: 'http://122.9.129.166:1918/detail/',
                method: "POST",
                data: {
                    name : this.interest,
                },
                //如果获取数据成功的话执行以下函数
                success: (resp) => {
                    //令获取到的数据赋给info
                    info = resp.data;
                    //console.log('景点信息'+ info)
                    //解码
                    info = JSON.parse(info)
                    //提示加载中
                    prompt.showToast({
                        message: "数据加载中...",
                        duration: 3000
                    });
                    this.picture = info["picture"],
                    //console.log('图片：'+ this.picture)

                    this.jianjie = info["jianjie"]
                    //console.log('简介：'+ this.jianjie)
                    //切换按钮为返回上一界面
                    this.but = "返回上一界面";
                },
                //如果获取数据失败则执行以下函数
                fail: (resp) => {
                    prompt.showToast({
                        message: "获取数据失败",
                        duration: 3000
                    });
                    //console.log("获取数据失败")
                }
            });
        }
        //按钮为返回上一界面时
        else{
            //console.log("发送"+this.select)
            router.place({
                uri: "pages/choo/choo",
            });
        }
    }
}