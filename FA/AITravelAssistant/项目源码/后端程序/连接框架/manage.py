#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

# 定义主函数
def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'goodluck.settings')
    try:
        # 导入框架
        from django.core.management import execute_from_command_line
        #出现异常，处理异常
    except ImportError as exc:
        raise ImportError(#不能导入相关模块时发出提醒
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
