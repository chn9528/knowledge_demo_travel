from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
import json 
import pandas as pd
from .good_luck import Play
from luck import arrange
from luck import interest


def login(request):   
    '''
    用户登录
    '''
    res={
        "i":"",
        "user":{
            "uid" : 0,
            "pwd" : "",
            "nickname": "",
            "occupation": 0,
            "age": 0,
            "gender": 0,
            "region": 0,
            "genre": 0                    
            }
        }
    json_data = request.body.decode("utf-8")
    data = json.loads(json_data)               #将json文件转换为字典
    print(data)
    '''
    测试数据
    '''
    # data ={
    #     "uid":'12129',
    #     "pwd":'123'
    #     }
    
    # 读取用户数据表
    users_title = ['uid','pwd', 'nickname','occupation','age', 'gender','region','genre']
    users = pd.read_table('D:\\tmp\\goodluck\\goodluck\\luck\\users.txt', sep=':', header=None, names=users_title, engine = 'python')
    # 比对用户输入信息
    try:
        uct = users.loc[users["uid"] == int(data["uid"])].index[0]
        if(str(users.iat[uct,1]) == str(data["pwd"])):
            res["user"]["uid"] = int(users.iat[uct,0])
            res["user"]["pwd"] = str(users.iat[uct,1])
            res["user"]["nickname"] = str(users.iat[uct,2])
            res["user"]["occupation"] = int(users.iat[uct,3])
            res["user"]["age"] = int(users.iat[uct,4])
            res["user"]["gender"] = int(users.iat[uct,5])
            res["user"]["region"] = int(users.iat[uct,6])
            res["user"]["genre"] = int(users.iat[uct,7])
            res["i"] = "登录成功"
        else:
            res["i"] = "输入账号或密码有误"        
    except IndexError:
        res["i"] = "账号不存在"
    print(res)
    return JsonResponse(res,safe=False)        #结果以json形式返回前端



def register(request):
    '''
    用户注册
    '''
    # 读取发送的数据
    json_data = request.body.decode("utf-8")
    mess = json.loads(json_data)               #将json文件转换为字典
    data ={
        'uid':mess.get('uid'),
        'pwd':mess.get('pwd'),
        'nickname':mess.get('nickname'),
        'occupation':mess.get('occupation'),
        'age':mess.get('age'),
        'gender':mess.get('gender'),
        'region':mess.get('region'),
        'genre':mess.get('genre')
        }
    print(data)
    res = {
        "注册信息":""
        }
    
    '''
    数据测试
    '''
    # data = {
    #     'uid':1,
    #     'pwd':1,
    #     'nickname':1,
    #     'occupation':1,
    #     'age':1,
    #     'gender':1,
    #     'region':1,
    #     'genre':1
    #     }
    # 将信息写入用户表
    f = open('D:\\tmp\\goodluck\\goodluck\\luck\\users.txt','a')  
    user = str(data['uid']) +':'+str(data['pwd'])+':'\
           +str(data['nickname'])+':'+str(data['occupation'])\
           +':'+str(data['age'])+':'+str(data['gender'])+':'\
           +str(data['region'])+':'+str(data['genre'])+'\n'
    suc = f.write(user)   
    f.close()
    # 是否成功写入
    if suc > 0:
        # print("已注册账号：",user)
        res["注册信息"] = "注册成功"
    else:
        # print("注册失败")
        res["注册信息"] = "注册失败，请重试"
    print(res)
    return JsonResponse(res,safe=False)
    

def run(request):    
    '''
    景点推荐
    '''    
    # 读取发送的数据
    json_data = request.body.decode("utf-8")
    data = json.loads(json_data)               #将json文件转换为字典
    print(data)
    
    # 调用编写的play类完成景点的推荐
    play=Play() 
    play.tfrun(data)                           #处理数据
    res=play.sss                              #得到推荐的景点
    print(res)
    return JsonResponse(res,safe=False)        #结果以json形式返回前端

  
def show(request):
    '''
    路线推荐
    '''  
    # 读取发送的数据
    json_data = request.body.decode("utf-8")
    data = json.loads(json_data)               #将json文件转换为字典    
    data = data.get('select')
    print(data)
    
    # 调用arrange中的route函数进行路线的规划
    res = arrange.route(data)                  #得到路线
    print(res)
    return JsonResponse(res,safe=False)        #结果以json形式返回前端

  
def detail(request):
    '''
    景点详情
    '''  
    # 读取发送的数据
    json_data = request.body.decode("utf-8")
    data = json.loads(json_data)               #将json文件转换为字典    
    print(data)
    data = data.get('name')
    
    # 调用interst中的showinterest函数进行景点具体数据的读取
    res = interest.showinterest(data)
    print(res)
    return JsonResponse(res,safe=False)        #结果以json形式返回前端
    