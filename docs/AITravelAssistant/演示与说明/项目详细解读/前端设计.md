前端设计
===========
**简要介绍**<br>
项目前端基于OpenHarmony系统，使用JS语言开发。
# 流程框架
![前端流程图](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/frontendFrame.png)

# 关键点解读
## 1、页面跳转
不同页面间的跳转以及数据传输
```js
/////登录页面跳转至主页面，其他与此类似，不一一展示
router.push({
    uri: "pages/home/home",
    params: {
        occupation: info["user"]["occupation"],
        age: info["user"]["age"],
        gender: info["user"]["gender"],
        region: info["user"]["region"],
        genre: info["user"]["genre"],

    }//params传输json格式数据给下一个界面，其他数据都会随着该界面的销毁而消失
```
## 2、循环展示
循环展示所有景点
```html
///hml文件，方案展示与此类似，在此不做展示
<list class="list">
    <list-item type="listItem" class="item" for="list" id="{{ $item.id }}"
               onclick="jumpPage({{ $item.id }}, {{ $item.uri }})">
        <input type="checkbox" value="{{ $item.title }}"@change="addChoice"></input>
        <text class="text">{{ $item.title }}</text>
    </list-item>
</list>
```
```js
///js文件，方案展示与此类似，在此不做展示
for (var i = 0; i < this.kaka.length; i++) {
    //解码转换成中文
    this.kaka[i] = unescape(this.kaka[i].replace(/\\u/gi,'%u'))
    //分别创建项目对象，可跳转至细节页面
    var item = {
        uri: "pages/detail/detail",
        title: this.kaka[i],
        id: i
    }
    this.index = this.index + 1;
    this.list.push(item); //将新创建的item对象添加到list列表中
}
```
## 3、应用签名
### a、使用命令行生成密钥和证书请求文件
OpenHarmony应用通过数字证书（.cer文件）和Profile文件（.p7b文件）来保证应用的完整性，
需要通过DevEco Studio来生成密钥文件（.p12文件）和证书请求文件（.csr文件）。
同时，也可以使用命令行工具的方式来生成密钥文件和证书请求文件。
在本项目中，使用的是命令行工具生成的方式。<br>
![打开任务管理器](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/cmd.png)

1. 使用**管理员身份**运行命令行工具
2. 切换到keytool工具所在路径，实际路径请根据安装目录进行修改。
3. 执行如下命令
```python
keytool -genkeypair -alias "lxxzs_key" -keyalg EC -sigalg SHA256withECDSA -dname "C=CN,O=StudentTeam,OU=StudentTeam,CN=lxxzs_key "  -keystore D:\APP\Huawei\DevEcoStudio\lxxzs\key\key.p12 -storetype pkcs12 -validity 365 -storepass 我的密码 -keypass 我的密码
```
生成证书请求文件的参数说明如下：
* alias：密钥的别名信息，用于标识密钥名称。**（记住！！）**
* sigalg：签名算法，固定为SHA256withECDSA。
* dname：按照操作界面提示进行输入。
	- C：国家/地区代码，如CN。
	- O：组织名称，如Organization。
	- OU：组织单位名称，如Unit。
	- CN：名字与姓氏，建议与别名一致。
* validity：证书有效期，建议设置为9125（25年）。
* storepass：设置密钥库密码，必须由大写字母、小写字母、数字和特殊符号中的两种以上字符的组合，
长度至少为8位。请记住该密码，后续签名配置需要使用。**（记住！！）**
* keypass：设置密钥的密码，请与storepass保持一致。**（记住！！）**

4. 执行如下命令，执行后需要输入storepass密码，生成证书请求文件，后缀格式为.csr
```python
keytool -certreq -alias "lxxzs_key " -keystore D:\APP\Huawei\DevEcoStudio\lxxzs\key\key.p12 -storetype pkcs12 -file D:\APP\Huawei\DevEcoStudio\lxxzs\key\request.cs
```
生成证书请求文件的参数说明如下：
* alias：与3中输入的alias保持一致。
* file：生成的证书请求文件名称，后缀为.csr。


### b、 生成应用证书文件
使用a步骤中生成的证书请求文件，来生成应用签名所需的数字证书文件。生成方法如下：
进入DevEco Studio安装目录的Sdk\toolchains\lib文件夹下（该SDK目录只能是OpenHarmony SDK），
打开命令行工具，执行如下命令（如果keytool命令不能执行，请在系统环境变量中添加JDK的环境变量）。
其中，只需要修改输入和输出即可快速生成证书文件，
即修改**-infile**指定证书请求文件csr文件路径**，-outfile**指定输出证书文件名及路径。


```python
keytool -gencert -alias "OpenHarmony Application CA" -infile D:\APP\Huawei\DevEcoStudio\lxxzs\key\request.csr -outfile D:\APP\Huawei\DevEcoStudio\lxxzs\key\zhengshu.cer -keystore OpenHarmony.p12 -sigalg SHA384withECDSA -storepass 123456 -ext KeyUsage:"critical=digitalSignature" -validity  365 -rfc
```
关于该命令的参数说明如下：
* alias：用于签发证书的CA私钥别名，OpenHarmony社区CA私钥存于OpenHarmony.p12密钥库文件中，该参数不能修改。
* infile：证书请求（CSR）文件的路径。
* outfile：输出证书链文件名及路径。
* keystore：签发证书的CA密钥库路径，OpenHarmony密钥库文件名为OpenHarmony.p12，
文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。
请注意，该OpenHarmony.p12文件并不是a步骤中生成的.p12文件。
* sigalg：证书签名算法，该参数不能修改。
* storepass：密钥库密码，密码为123456，该参数不能修改。
* ext：证书扩展项，该参数不能修改。
* validity：证书有效期，自定义天数。
* rfc：输出文件格式指定，该参数不能修改

### c、 生成应用Profile文件
Profile文件包含OpenHarmony应用的包名、数字证书信息、描述应用允许申请的证书权限列表，
以及允许应用调试的设备列表（如果应用类型为Release类型，则设备列表为空）等内容，
每个应用包中均必须包含一个Profile文件。

<br>进入Sdk\toolchains\lib目录下，打开命令行工具，执行如下命令
```java
java -jar provisionsigtool.jar sign --in UnsgnedReleasedProfileTemplate.json --out D:\APP\Huawei\DevEcoStudio\lxxzs\key\profile.p7b --keystore OpenHarmony.p12 --storepass 123456 --alias "OpenHarmony Application Profile Release" --sigAlg SHA256withECDSA --cert OpenHarmonyProfileRelease.pem --validity 365 --developer-id ohosdeveloper --bundle-name 包名 --permission 受限权限名（可选） --permission 受限权限名（可选） --distribution-certificate D:\APP\Huawei\DevEcoStudio\lxxzs\key\zhengshu.cer
```
关于该命令的参数说明如下：
* provisionsigtool：Profile文件生成工具，文件在OpenHarmony SDK的Sdk\toolchains\lib路径下。
* in：Profile模板文件所在路径，文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。
* out：输出的Profile文件名和路径。
* keystore：签发证书的密钥库路径，OpenHarmony密钥库文件名为OpenHarmony.p12，文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。
* storepass：密钥库密码，密码为123456，该参数不能修改。
* alias：用于签名Profile私钥别名，OpenHarmony社区CA私钥存于OpenHarmony.p12密钥库文件中，该参数不能修改。
* sigalg：证书签名算法，该参数不能修改。
* cert：签名Profile的证书文件路径，文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。
* validity：证书有效期，自定义天数。
* developer-id：开发者标识符，自定义一个字符串。
* bundle-name：填写应用包名。
* permission：可选字段，如果不需要，则可以不用填写此字段；
如果需要添加多个受限权限，则如示例所示重复输入。
受限权限列表如下：ohos.permission.READ_CONTACTS、ohos.permission.WRITE_CONTACTS。
* distribution-certificate：b步骤中生成的证书文件。

### d、 配置应用签名信息
在真机设备上调试前，需要使用到制作的私钥（.p12）文件、证书（.cer）文件和Profile（.p7b）文件对调试的模块进行签名。
打开File > Project Structure，点击Project > Signing Configs > debug窗口中，去除勾选“Automatically generate signing”，然后配置指定模块的调试签名信息。
* Store File：选择密钥库文件，文件后缀为.p12，该文件为a步骤中生成的.p12文件。
* Store Password：输入密钥库密码，该密码为a步骤中填写的密钥库密码保持一致。
* Key Alias：输入密钥的别名信息，与a步骤中填写的别名保持一致。
* Key Password：输入密钥的密码，与Store Password保持一致。
* Sign Alg：签名算法，固定为SHA256withECDSA。
* Profile File：选择c步骤中生成的Profile文件，文件后缀为.p7b。
* Certpath File：选择b步骤中生成的数字证书文件，文件后缀为.cer。
![签名](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/signature.png)