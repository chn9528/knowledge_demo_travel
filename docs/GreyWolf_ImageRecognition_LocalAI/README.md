# [智能停车系统之车牌识别器（OpenCV版本）](../../dev/team_x/GreyWolf_ImageRecognition_LocalAI)

## 简介

本文介绍如何在OpenHarmony 3.1 Release 小型系统上[实现车牌识别](../../dev/team_x/GreyWolf_ImageRecognition_LocalAI)。本样例通过语音播放、相机拍照、opencv识别车牌、获取识别结果并通过串口显示。

![](media/display.gif)

## 快速上手

### 准备硬件环境

润和HiSpark Taurus AI Camera(Hi3516d)开发板套件；

准备8GSD卡插入Hi3516DV300插槽；

### 准备开发环境

####  安装必备软件

开发基础环境由windows 工作台和Linux  编译服务器组成。windows 工作台可以通过samba 服务或ssh  方式访问Linux编译服务器。其中windows  工作台用来烧录和代码编辑，Linux编译服务器用来编译OpenHarmony代码，为了简化步骤，Linux编译服务器推荐安装Ubuntu20.04。

##### 安装和配置Python 

- 打开Linux终端。

- 输入如下命令，查看python版本号，需要使用python3.7以上版本,否则参考 [系统基础环境搭建](https://gitee.com/openharmony/docs/blob/OpenHarmony_1.0.1_release/zh-cn/device-dev/quick-start/搭建系统基础环境.md)。

  ```
  python3 --version
  ```

- 安装并升级Python包管理工具（pip3）。

  ```
  sudo apt-get install python3-setuptools python3-pip -y
  sudo pip3 install --upgrade pip
  ```

##### 安装hb 

- 运行如下命令安装hb

  ```
  python3 -m pip install --user ohos-build
  ```

- 将hb加入系统环境变量

  ```
  vim ~/.bashrc
  ```

  将以下命令拷贝到.bashrc文件的最后一行，保存并退出。

  ```
  export PATH=~/.local/bin:$PATH
  ```

  执行如下命令更新环境变量。

  ```
  source ~/.bashrc
  ```

- 测试hb安装结果 执行"hb -h"，有打印以下信息即表示安装成功.

  ```
  usage: hb
  OHOS build system
  positional arguments:
    {build,set,env,clean}
      build               Build source code
      set                 OHOS build settings
      env                 Show OHOS build env
      clean               Clean output
  optional arguments:
    -h, --help            show this help message and exit
  ```

- hb版本替换

  本样例hb版本为0.2.0，如果当前hb版本不是0.2.0，需要替换hb版本，步骤如下(该步骤必须在openharmony的工程根目录下执行)：

  ```
  pip3 uninstall ohos_build
  pip3 install build/lite
  ```

####  安装交叉编译环境

在Linux编译服务器上搭建好基础开发环境后，需要安装OpenHarmony 编译Hi3518平台特有的开发环境。

##### 将Linux shell改为bash

查看shell是否为bash，在终端运行如下命令

```
ls -l /bin/sh
```

如果显示为“/bin/sh -> bash”则为正常，否则请按以下方式修改：

```
sudo rm -rf /bin/sh
sudo ln -s /bin/bash /bin/sh
```

#####  安装编译依赖基础软件 

```
sudo apt-get install build-essential && sudo apt-get install gcc && sudo apt-get install g++ && sudo apt-get install make && sudo apt-get install zlib* && sudo apt-get install libffi-dev
```

##### 安装文件打包工具 

- 安装dosfstools

  ```
  sudo apt-get install dosfstools
  ```

- 安装mtools

  ```
  sudo apt-get install mtools
  ```

- 安装mtd-utils

  ```
  sudo apt-get install mtd-utils
  ```

##### 安装hc-gen 

- 下载hc-gen工具：[下载地址](https://gitee.com/link?target=https%3A%2F%2Frepo.huaweicloud.com%2Fharmonyos%2Fcompiler%2Fhc-gen%2F0.65%2Flinux%2Fhc-gen-0.65-linux.tar)。

- 解压hc-gen安装包到Linux服务器~/hc-gen路径下。

  ```
  tar -xvf hc-gen-0.65-linux.tar -C ~/
  ```

- 设置环境变量。

  ```
  vim ~/.bashrc
  ```

  将以下命令拷贝到.bashrc文件的最后一行，保存并退出。

  ```
  export PATH=~/hc-gen:$PATH
  ```

- 使环境变量生效。

  ```
  source ~/.bashrc
  ```

##### 安装LLVM 

**openharmony3.1及之后的版本不需要手动安装LLVM，可跳过此步骤。**

- 下载指定的LLVM工具：[下载地址](https://gitee.com/link?target=https%3A%2F%2Frepo.huaweicloud.com%2Fharmonyos%2Fcompiler%2Fclang%2F9.0.0-36191%2Flinux%2Fllvm-linux-9.0.0-36191.tar)。

- 解压LLVM安装包至~/llvm路径下。

  ```
  tar -zxvf llvm.tar -C ~/
  ```

- 设置环境变量。

  ```
  vim ~/.bashrc
  ```

  将以下命令拷贝到.bashrc文件的最后一行，保存并退出。

  ```
  export PATH=~/llvm/bin:$PATH
  ```

- 使环境变量生效。

  ```
  source ~/.bashrc
  ```

#### 准备工程

本用例采用repo的方式从码云官仓下载系统系统源码以及开发板适配代码，使用git从gitee的sig仓库拉取设备应用代码。

##### 配置git

- 提前注册准备码云gitee账号。
- git工具下载安装

```
sudo apt install git
sudo apt install git-lfs
```

- 生成/添加SSH密钥：生成密钥 使用gitee账号绑定的邮箱生成密钥对

```
ssh-keygen -t ed25519 -C "xxxxx@xxxxx.com"
```

- 查看生成的密钥

```
cat ~/.ssh/id_ed25519.pub
```

- 复制生成后的 ssh key，返回gitee个人主页，通过主页 「个人设置」->「安全设置」->「SSH 公钥」 ，将生成的“SSH密钥”添加到仓库中。
- 配置git用户信息

```
git config --global user.name "yourname"
git config --global user.email "your-email-address"
git config --global credential.helper store
```

#####  准备repo

```
curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo  ## 如果没有权限可以，可先将repo下载到当前目录在拷贝
chmod a+x /usr/local/bin/repo
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple requests
```

#####  准备系统源码

本样例是基于OpenHarmony3.2 Beta版本开发的，所以需要下载OpenHarmony3.2 Beta版。

```
mkdir ~/OpenHarmony3.2
cd ~/OpenHarmony3.2
repo init -u git@gitee.com:openharmony/manifest.git -b OpenHarmony-3.2-Beta1 --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

#####  准备开发板适配代码

具体仓库地址: [knowledge_demo_travel](https://gitee.com/openharmony-sig/knowledge_demo_travel)

通过git命令下载并拷贝道：OpenHarmony对应目录

```
cd ~
git clone https://gitee.com/openharmony-sig/knowledge_demo_travel.git --depth=1
cp -raf knowledge_demo_travel/dev/teamx/GreyWolf_ImageRecognition_LocalAI/ImageRecognition_LocalAI ~/OpenHarmony3.2/applications/sample/camera/ 
```

##### 准备三方库zxing-cpp代码

```
cd ~
git clone git@gitee.com:zhong-luping/ohos_zxing_cpp.git
cp -raf ohos_zxing_cpp/zxing-cpp ~/OpenHarmony3.1/third_party/
```

##### 准备EasyPR代码

```
cd ~
git clone git@gitee.com:zhong-luping/ohos_easy-pr.git
cp -raf ohos_easy-pr/EasyPR ~/OpenHarmony3.2/third_party/
```

##### 准备OpenCV代码

```
cd ~
git clone git@gitee.com:zhong-luping/ohos_opencv.git
cp -raf ohos_opencv/opencv ~/OpenHarmony3.2/third_party/
```

特别申明：

- 以上三方库都未上主仓，暂时用私仓替代，上主仓后必需用主仓地址。
- 以上三方库都是已适配了OHOS编译系统，对三方库移植有兴趣的可以参考[L1上运行开源项目车牌识别及移植opencv库](../GreyWolf_EasyPR/readme.md)

##### 配置应用自启动

更改启动文件init_liteos_a_3518ev300.cfg将应用设置为开机自启。

修改文件vendor/hisilicon/hispark_taurus/init_configs/init_liteos_a_3516dv300.cfg

在init字段中添加"start localAI_sample"

```
            "name" : "init",
            "cmds" : [
                "start apphilogcat",
                "start hiview",
                "start deviceauth_service",
                "start sensor_service",
                "start ai_server",
                "start softbus_server",
                "start localAI_sample"//添加
            ]
```

在server字段中添加服务

```
		{
            "name" : "localAI_sample",
            "path" : ["/bin/localAI_sample"],
            "uid" : 10,
            "gid" : 10,
            "once" : 1,
            "importance" : 0,
            "caps" : [4294967295]
        }
```

##### 应用添加到编译系统

更改applications.json与config.json将应用加入到编译体系中来

build/lite/components/applications.json文件添加应用：

```
    {
      "component": "localAI",
      "description": "localAI samples.",
      "optional": "true",
      "dirs": [
        "applications/sample/camera/ImageRecognition_LocalAI"
      ],
      "targets": [
        "//applications/sample/camera/ImageRecognition_LocalAI:ImageRecognition_LocalAI"
      ],
      "rom": "",
      "ram": "",
      "output": [],
      "adapted_kernel": [ "liteos_a" ],
      "features": [],
      "deps": {
        "components": [],
        "third_party": [ ]
      }
    },
```

vendor/hisilicon/hispark_taurus/config.json文件applications子系统中添加编译构建：

```
      {
        "subsystem": "applications",
        "components": [
          { "component": "camera_sample_app", "features":[] },
          { "component": "camera_sample_ai", "features":[] },
          { "component": "localAI", "features":[] },//添加
          { "component": "camera_screensaver_app", "features":[] }
        ]
      },
```



### 编译

 进入到OpenHarmony系统源码根目录下，输入hb set并选择ipcamera_hispark_taurus

```
hb set  // 如果是第一次编译，Input code path 命令行中键入"./" 指定OpenHarmony工程编译根目录后 回车。
hb build -f  // 开始全量编译。（hb build 为增量编译）
```

### 固件烧录

#### 烧录工具选择

固件编译完后，是需要烧录到单板的。这里我们用的是HiTool工具烧录的。(HiTool工具下载地址:[HiHope官网](https://gitee.com/link?target=http%3A%2F%2Fwww.hihope.org%2Fdownload%2Fdownload.aspx%3Fmtt%3D33))

#### 烧录步骤

打开HiTool工具，如下图：

烧写步骤按照图中标注即可。点击擦除后再拔出USB口再接入。![hitool_config](media/hitool_config.png)

最后重新上电即可烧录。

![](media/hitoo_burn.png)

烧录成功后，使用串口连接。输入如下启动参数。

```
setenv bootcmd "mmc read 0x0 0x80000000 0x800 0x4800; go 0x80000000";
setenv bootargs "console=ttyAMA0,115200n8 root=emmc fstype=vfat rootaddr=10M rootsize=60M rw";
save
reset
```

### 启动应用

#### 准备

将源码下的sdcard文件拷贝SD卡中。

在烧录前使用本地应用相机调整焦距，将摄像头调整识别二维码为距离为25cm。

#### 车牌识别

将摄像头对准车牌

![](media/licensePlate.png)

在串口中输入1拍照、输入2将照片提交至百度云进行识别，最终返回识别结果。

![](media/getLicensePlate.png)

## 样例联动

本样例可与其他智能停车场样例组合产品联动效果，参考 [智能停车场](../GreyWolf_SmartParking/README.md)

## 参考资料

- [系统基础环境搭建](https://gitee.com/openharmony/docs/blob/OpenHarmony_1.0.1_release/zh-cn/device-dev/quick-start/搭建系统基础环境.md)
- [智慧出行场景样例](https://gitee.com/openharmony-sig/knowledge_demo_travel)
- [智能停车场介绍](../GreyWolf_SmartParking/README.md)
- [L1上运行开源项目车牌识别及移植opencv库](../GreyWolf_EasyPR/readme.md)

- [知识体系](https://gitee.com/openharmony-sig/knowledge)

