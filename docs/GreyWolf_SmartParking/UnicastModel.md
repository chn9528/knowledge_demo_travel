## 单播通信模型

### 数据结构

#### 发送结构

```
{
	messageType: "command",
	messageId: 0, // 0~255
	message: {
		publish: '发布的消息类型',
		params:{
			key:'value'
		}
	}
}
```

#### 响应结构

```
{
	messageType: "response",
	messageId: 0, // 0~255
	message: {
	    id：'xxxx',
	    result：'xx',
	    state:200 
	}
}
```

result：英文结果，方便开发查看

state枚举值

- 200 正常
- 500 异常

### 发布能力

#### 车辆靠近 carApproachDetect

```
{
	messageType: "command",
	messageId: 3, // 0~255
	message: {
        publish:"carApproachDetect",
        params:{
            detectResult:'leave'
        }
    }
}
```

detectResult枚举值

- near 车辆靠近中 (通知一次，且目前暂不做处理)
- leave 车辆已离开
- waiting 车辆已门前就位(到达一定范围内)

#### 门禁控制 latchControl

```
{
	messageType: "command",
	messageId: 45, // 0~255
	message: {
        publish:"latchControl",
        params:{
            operate:"open"
        }
    }
}
```

operate枚举值

- open 打开
- close 关闭

#### 车牌识别 licensePlateRecognition

```
{
	messageType: "command",
	messageId: 77, // 0~255
	message: {
        publish:"licensePlateRecognition",
        params:{
            licensePlate:"粤Bxxxxxx"，
            recognitionState:200
        }
    }
}
```

recognitionState枚举值

- 200 正常
- 500 识别错误

#### 门禁状态改变 latchChange

```
{
	messageType: "command",
	messageId: 174, // 0~255
	message: {
        publish:"latchChange",
        params:{
            latchState:"open"
        }
    }
}
```

latchState枚举值

- open 打开
- close 关闭

