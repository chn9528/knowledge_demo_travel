/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EXT_DEV_IF_H__
#define __EXT_DEV_IF_H__

#include <cmsis_os2.h>

/// 外部设备控制接口返回值
typedef enum {
    EXT_DEV_SUCCESS = 0,
    EXT_DEV_ERROR
} ExtDevCtlRst;

/// led灯控制定义
typedef enum {
    EXT_DEV_LED_GREEN = 0,
    EXT_DEV_LED_BLUE,
    EXT_DEV_LED_RED
} ExtDevLedIdDef;

typedef enum {
    EXT_DEV_LED_ON = 0,
    EXT_DEV_LED_OFF
} ExtDevLedActDef;

/// 蜂鸣器控制定义
typedef enum {
    EXT_DEV_BEEP_ON = 0,
    EXT_DEV_BEEP_OFF
} ExtDevBeepActDef;

/// 人体红外传感器定义
typedef enum {
    EXT_DEV_IR_OBTAIN = 0,
    EXT_DEV_IR_NOTICE
} ExtDevIrWorkTypeDef;

typedef enum {
    EXT_DEV_IR_HAVE_MAN = 0,
    EXT_DEV_IR_NO_MAN
} ExtDevIrMontStsDef;
typedef void (*ExtDevIrStsNoticeCbDef)(ExtDevIrMontStsDef extDevIrMontSts);

typedef struct {
    ExtDevIrWorkTypeDef extDevIrWorkType;
    ExtDevIrStsNoticeCbDef extDevIrStsNoticeCb;
} ExtDevIrCfgDef;

/// 按键定义
/// led灯控制定义
typedef enum {
    EXT_DEV_KEY_0 = 0,
    EXT_DEV_KEY_1,
    EXT_DEV_KEY_2,
    EXT_DEV_KEY_3
} ExtDevKeyIdDef;

typedef enum {
    EXT_DEV_KEY_OBTAIN = 0,
    EXT_DEV_KEY_NOTICE
} ExtDevKeyWorkTypeDef;

typedef uint16_t ExtDevKeyNtFilterMsDef;

typedef enum {
    EXT_DEV_KEY_UNPRESS = 0,
    EXT_DEV_KEY_PRESSED
} ExtDevKeyMontStsDef;

typedef void (*ExtDevKeyStsNoticeCbDef)(ExtDevKeyMontStsDef extDevKeyMontSts);

typedef struct {
    ExtDevKeyIdDef extDevKeyId;
    ExtDevKeyWorkTypeDef extDevKeyWorkType;
    ExtDevKeyStsNoticeCbDef extDevKeyStsNoticeCb;
} ExtDevKeyCfgDef;

/// 超声波测距模块工作状态
typedef enum {
    ULT_RANG_ST_CLOSE = 0,
    ULT_RANG_ST_STANDBY = 1,
    ULT_RANG_ST_MEASURING = 2,
    ULT_RANG_ST_MEASURED = 3,
    ULT_RANG_ST_ERROR = -1
} ExtDevHcSr04State;

/// led灯初始化
ExtDevCtlRst ExtDevLedInit(void);
/// led灯初始化
ExtDevCtlRst ExtDevLedInitById(ExtDevLedIdDef extDevLedId);
/// led灯开关控制
ExtDevCtlRst ExtDevLedSetSwitch(ExtDevLedIdDef extDevLedId, ExtDevLedActDef extDevLedAct);
/// led灯闪烁控制
ExtDevCtlRst ExtDevLedSetBlinking(ExtDevLedIdDef extDevLedId, uint32_t ms);

/// 蜂鸣器灯初始化
ExtDevCtlRst ExtDevBeepInit(void);
/// 蜂鸣器控制
ExtDevCtlRst ExtDevBeepSet(ExtDevBeepActDef extDevBeepAct);
/// 蜂鸣器控制
ExtDevCtlRst ExtDevBeepSetFreq(uint32_t ms);

/// 人体红外传感器初始化
ExtDevCtlRst ExtDevIrInit(ExtDevIrCfgDef extDevIrCfg);
/// 人体红外传感器监测结果获取
ExtDevIrMontStsDef ExtDevGetIrMonSts(void);

/// 按键初始化
ExtDevCtlRst ExtDevKeyInit(ExtDevKeyCfgDef *extDevKeyCfg, int keyNum);
/// 按键状态获取
ExtDevKeyMontStsDef ExtDevGetKeySts(ExtDevKeyIdDef extDevKeyId);

int ExtDevKeyIsPress(ExtDevKeyIdDef keyId);

/// 伺服电机初始化
ExtDevCtlRst ExtDevSg90Init(void);
/// 伺服电机关闭
ExtDevCtlRst ExtDevSetSg90Close(void);
/// 伺服电机控制 0° ~ 180°
ExtDevCtlRst ExtDevSetSg90Angle(uint8_t rotationAngle);

///超声波测距模块初始化
ExtDevCtlRst ExtDevHcSr04Init(void);
/// 超声波测距开始
ExtDevCtlRst ExtDevHcSr04Start(void);
/// 获取超声波测距模块工作状态
ExtDevHcSr04State ExtDevGetHcSr04State(void);
/// 获取超声波测距结果
int32_t ExtDevGetHcSr04MsrRst(void);

#ifdef __cplusplus
}
#endif

#endif // __EXT_DEV_IF_H__
