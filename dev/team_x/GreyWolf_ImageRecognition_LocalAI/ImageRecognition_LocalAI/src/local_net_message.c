/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include <securec.h>

#include "log.h"
#include "local_net_message.h"

static pthread_mutex_t g_localNetComMutex;
LocalNetMsgRecv s_data;
static int s_ataLen = 0;

int LocalNetComMsgRcv(LocalNetMsgRecv *data)
{
    if (s_data.dataLen == 0) {
        SAMPLE_ERROR("buff don't have data");
        return -1;
    }
    (void)memcpy_s(data->deviceId, sizeof(data->deviceId), s_data.deviceId, sizeof(s_data.deviceId));
    data->dataLen = s_data.dataLen;
    data->dataType = s_data.dataType;
    (void)memcpy_s(data->data, sizeof(data->data), s_data.data, sizeof(s_data.data));
    SAMPLE_INFO("s_data->%s", s_data.data);
    (void)memset_s(&s_data, sizeof(LocalNetMsgRecv), 0, sizeof(LocalNetMsgRecv));
    pthread_mutex_unlock(&g_localNetComMutex);
    return 0;
}

int LocalNetComMsgSnd(LocalNetMsgRecv *data)
{
    pthread_mutex_lock(&g_localNetComMutex);
    (void)memcpy_s(s_data.deviceId, sizeof(s_data.deviceId), data->deviceId, sizeof(data->deviceId));
    s_data.dataLen = data->dataLen;
    s_data.dataType = data->dataType;
    (void)memcpy_s(s_data.data, sizeof(s_data.data), data->data, sizeof(data->data));

    return 0;
}

static LocalNetUdpSendDataDef s_NetUdpData;
static pthread_mutex_t g_localNetCtlMutex;

int LocalNetCtlMsgRcv(LocalNetUdpSendDataDef *data)
{
    if (s_NetUdpData.dataLen == 0) {
        return -1;
    }
    (void)memcpy_s(data, sizeof(LocalNetUdpSendDataDef), &s_NetUdpData, sizeof(LocalNetUdpSendDataDef));
    SAMPLE_INFO("data->%s", data->data);
    (void)memset_s(&s_NetUdpData, sizeof(LocalNetUdpSendDataDef), 0, sizeof(LocalNetUdpSendDataDef));
    pthread_mutex_unlock(&g_localNetCtlMutex);
    return 0;
}

int LocalNetCtlMsgSnd(LocalNetUdpSendDataDef *data)
{
    SAMPLE_INFO("ip->%s", inet_ntoa(*(struct in_addr *)(&(data->netData.ip))));
    pthread_mutex_lock(&g_localNetCtlMutex);
    (void)memcpy_s(&s_NetUdpData, sizeof(LocalNetUdpSendDataDef), data, sizeof(LocalNetUdpSendDataDef));
    SAMPLE_INFO("data->%s", s_NetUdpData.data);
    return 0;
}

int MstInit(void)
{
    int ret;

    (void)memset_s(&s_data, sizeof(LocalNetMsgRecv), 0, sizeof(LocalNetMsgRecv));
    (void)memset_s(&s_NetUdpData, sizeof(LocalNetUdpSendDataDef), 0, sizeof(LocalNetUdpSendDataDef));
    ret = pthread_mutex_init(&g_localNetComMutex, NULL);
    if (ret != 0) {
        SAMPLE_ERROR("pthread_mutex_init");
        return -1;
    }
    ret = pthread_mutex_init(&g_localNetCtlMutex, NULL);
    if (ret != 0) {
        SAMPLE_ERROR("pthread_mutex_init");
        return -1;
    }

    return 0;
}

void MsgExit(void)
{
    int ret = pthread_mutex_destroy(&g_localNetComMutex);
    if (ret != 0) {
        printf("[ERROR]pthread_mutex_destroy ret -> %d \n", ret);
    }

    ret = pthread_mutex_destroy(&g_localNetCtlMutex);
    if (ret != 0) {
        printf("[ERROR]pthread_mutex_destroy ret -> %d \n", ret);
    }
}
