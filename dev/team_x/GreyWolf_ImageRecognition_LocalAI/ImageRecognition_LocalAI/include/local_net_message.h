/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LOCAL_NET_MESSAGE_H
#define LOCAL_NET_MESSAGE_H

#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <errno.h>

#include "local_net_def.h"

#define MSG_SZ 1024

typedef struct {
    char deviceId[DEVICE_ID_LEN];
    uint16_t dataLen;
#ifdef L2_DEVICE
    long dataType;
#endif
    char data[MSG_SZ];
} LocalNetMsgRecv;


typedef struct {
#ifdef L0_DEVICE
    struct in_addr ip;
    in_port_t port;
#elif defined(L2_DEVICE)
    in_addr_t ip;
    uint16_t port;
#endif
} NetData;

typedef struct {
    NetData netData;
    uint32_t dataLen;
    char data[MSG_SZ];
#ifdef L2_DEVICE
    long dataType;
#endif
} LocalNetUdpSendDataDef;

int LocalNetComMsgSnd(LocalNetMsgRecv *data);
int LocalNetComMsgRcv(LocalNetMsgRecv *data);

int LocalNetCtlMsgRcv(LocalNetUdpSendDataDef *data);
int LocalNetCtlMsgSnd(LocalNetUdpSendDataDef *data);
int MstInit(void);
void MsgExit(void);
#endif