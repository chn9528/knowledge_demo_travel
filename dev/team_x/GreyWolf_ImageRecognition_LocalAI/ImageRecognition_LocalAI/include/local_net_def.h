/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LOCAL_NET_DEF_H
#define LOCAL_NET_DEF_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define L2_DEVICE 1
#define L1_DEVICE

#ifdef L0_DEVICE
#include "hilog/log.h"
#include "hilog/hiview_log.h"

#undef LOG_TAG
#define LOG_TAG "LOCAL_NET"
#define LOG_D(fmt, ...) HILOG_DEBUG(HILOG_MODULE_APP, "[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_W(fmt, ...) HILOG_WARN(HILOG_MODULE_APP, "[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_I(fmt, ...) HILOG_INFO(HILOG_MODULE_APP, "[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_E(fmt, ...) HILOG_ERROR(HILOG_MODULE_APP, "[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define DBG_ASSERT(cond)                                                             \
    do {                                                                             \
        if (!(cond)) {                                                               \
            HILOG_ERROR("%s : %d '%s' assert failed.\r\n", __FILE__, __LINE__, #cond); \
            while (1)                                                                \
                ;                                                                    \
        }                                                                            \
    } while (0)
#else
#ifdef L1_DEVICE
#include "log.h"
#define addLogs(arg) SAMPLE_INFO("%s", arg)
#define LOG_D(fmt, ...) SAMPLE_INFO("[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_W(fmt, ...) SAMPLE_INFO("[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_I(fmt, ...) SAMPLE_INFO("[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_E(fmt, ...) SAMPLE_ERROR("[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define DBG_ASSERT(cond)                                                            \
                do {                                                                             \
                    if (!(cond)) {                                                                 \
                        LOG_E("'%s' assert failed.\r\n", #cond); \
                        while (1)                                                                 \
                            ;                                                                     \
                    }                                                                             \
                } while (0)


#else
#include "hilog/log_c.h"

#define LOG_D(fmt, ...) HILOG_DEBUG(LOG_APP,  "[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_W(fmt, ...) HILOG_WARN(LOG_APP, "[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_I(fmt, ...) HILOG_INFO(LOG_APP, "[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_E(fmt, ...) HILOG_ERROR(LOG_APP, "[%s] %d : " fmt " \r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define DBG_ASSERT(cond)                                                            \
        do {                                                                             \
            if (!(cond)) {                                                               \
                LOG_E("'%s' assert failed.\r\n", #cond); \
                while (1)                                                                \
                    ;                                                                    \
            }                                                                            \
        } while (0)
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define DEVICE_ID_LEN 65
#define MQ_MSG_SIZE_MAX 4
#define MQ_MSG_NUM_MAX 10

#define BUFF_SHORT_SIZE (32)
#define BUFF_LONG_SIZE  (256)
#define ID_SIZE         (64)
#define SEND_RETRY_TIME (5)

#define USLEEP_300      (300000)
#define OS_DELAY_300    (300)

#ifdef L0_DEVICE
#define STACK_PAGE      (1024)
#define STACK_SIZE      (STACK_PAGE * 24)
#define STACK_SIZE_8    (STACK_PAGE * 8)
#define STACK_SIZE_4    (STACK_PAGE * 4)
#define STACK_PRIORITY  (24)
#endif

enum ResponseState {
    /**
     * 控制命令响应状态值.
     */
    SUCCESS = 200,
    ERROR = 500
};

/****************
 * 功能：显示程序异常所在位置、内容的宏
 * 参数：x(待比较的第一个参数), option(比较符号如：> =), y(待比较的第二个参数)，message(自定义的异常内容)， z(自定义的返回值)
 * 返回值: z ：NULL 表示数据为空，-1表示其他异常
 * *************/
#define SYSERR(x, option, y, message, z)    if ((x) option (y)) \
{ \
    printf("\t%s %s %d : error: %s\n", __FILE__, __func__, __LINE__, message); \
    return z; \
}

typedef enum {
    LOCAL_NET_THREAD_INIT = 0,
    LOCAL_NET_THREAD_RUNNING,
    LOCAL_NET_THREAD_RELEASE,
    LOCAL_NET_THREAD_EXIT,
    LOCAL_NET_THREAD_ERR
} LocalNetThreadStatus;

#ifdef __cplusplus
}
#endif

#endif
