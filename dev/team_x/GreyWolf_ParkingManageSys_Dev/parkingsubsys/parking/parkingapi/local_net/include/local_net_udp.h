/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef __cplusplus 
extern "C" { 
#endif

#ifndef __LOCAL_NET_UDP_H__
#define __LOCAL_NET_UDP_H__

#include "local_net_def.h"
#include "local_net_communication.h"
#ifdef L0_DEVICE
#include <cmsis_os2.h>
#include "log.h"
#endif

#ifdef L2_DEVICE
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <string.h>
#endif

#define MSG_SZ 1024

typedef struct {
#ifdef L0_DEVICE
    struct in_addr ip;
    in_port_t port;
#elif defined(L2_DEVICE)
    in_addr_t ip;
    uint16_t port;
#endif
} NetData;

typedef struct {
#ifdef L2_DEVICE
   long dataType;
#endif
    char deviceId[DEVICE_ID_LEN];
    uint16_t dataLen;
    char data[MSG_SZ];
} LocalNetMsgRecv;

typedef struct {
#if defined(L2_DEVICE)
   long dataType;
#endif
    NetData netData;
    uint32_t dataLen;
    char data[MSG_SZ];
} LocalNetUdpSendDataDef;

typedef struct {
    key_t key;
    int msgQueue;
} LocalQueInfo;

typedef struct {
    long type;
    LocalNetUdpSendDataDef data;
} MsgInfo;

typedef struct {
    long type;
    LocalNetMsgRecv data;
} MsgInfoComm;

/*
 * LocalNetUdpRecvCb
 */
typedef int8_t (*LocalNetUdpRecvCb)(const char *recvMsg, const char *deviceId);

/*
 * Time2Tick
 */
uint32_t Time2Tick(uint32_t ms);

/*
 * LocalNetUdpInit
 */
int8_t LocalNetUdpInit(void);

/*
 * LocalNetUdpDeinit
 */
int8_t LocalNetUdpDeinit(void);

/*
 * LocalNetUdpSend
 */
int8_t LocalNetUdpSend(const char *msg, const char *deviceID);

/*
 * LocalNetUdpRecvCbReg
 */
int8_t LocalNetUdpRecvCbReg(LocalNetUdpRecvCb recvCb);

/*
 * LocalNetUdpBoardcast
 */
int8_t LocalNetUdpBoardcast(const char *msg, uint8_t interval);

/**
 * @brief Get the Local Mac Addr object
 * 
 * @param ethName 
 * @param macAddr 
 * @param macSize 
 * @return int 
 */
int getLocalMacAddr(const char *ethName, char *macAddr, int macSize);

#endif // __LOCAL_NET_UDP_H__

#ifdef __cplusplus 
} 
#endif 