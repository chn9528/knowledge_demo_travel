/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import display from '@ohos.display'

const SYSTEM_UI_HEIGHT = 134
const FOOTER_UI_HEIGHT = 160
const DESIGN_WIDTH = 720.0
const PREVIEW_ASPECT_RATIO = 0.75

export default class PreviewModel {
    screenStyle = {}

    constructor() {
    }

    getAppStyle(callback) {
        let self = this;
        display.getDefaultDisplay().then((dis) => {
            self.getScreenSize(dis)
            console.log('PreviewModel getDefaultDisplay screenStyle -- ' + JSON.stringify(self.screenStyle))
            callback(self.screenStyle)
        })
    }

    getScreenSize(dis) {
        let self = this
        let disWidth = dis.width
        let disHeight = dis.height
        console.log('PreviewModel getScreenSize disWidth -- ' + disWidth)
        console.log('PreviewModel getScreenSize disHeight -- ' + disHeight)

        let proportion = DESIGN_WIDTH / disWidth
        self.screenStyle.height = (dis.height - SYSTEM_UI_HEIGHT) * proportion
        self.screenStyle.width = DESIGN_WIDTH

        self.getPreviewStyle()
    }

    getPreviewStyle() {
        let self = this
        let screenWidth = self.screenStyle.width
        let screenHeight = self.screenStyle.height
        if (screenWidth / screenHeight <= PREVIEW_ASPECT_RATIO) {
            console.log('PreviewModel getPreviewStyle  without margin')
            self.screenStyle.previewWidth = screenWidth
            self.screenStyle.previewHeight
            = self.screenStyle.previewWidth / PREVIEW_ASPECT_RATIO
            self.screenStyle.footerWrapMargin = 0
            self.screenStyle.footerHeight = 255
        } else {
            console.log('PreviewModel getPreviewStyle left and right margin')
            self.screenStyle.previewHeight = screenHeight
            self.screenStyle.previewWidth
            = self.screenStyle.previewHeight * PREVIEW_ASPECT_RATIO
            self.screenStyle.footerWrapMargin = (screenWidth - self.screenStyle.previewWidth) / 2
            self.screenStyle.footerHeight = 55
        }
    }
}