/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "log.h"
#include "camera.h"
#include "wpa_work.h"
#include "local_net_udp.h"
#include "local_net_communication.h"
#include "http.h"
#include "cJSON.h"

#include <algorithm>
#include <cstring>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

using namespace std;
static bool  gRunAiSwitch = false;

static int8_t msgRecv(const char *msg)
{
	static int runThread = 0;
	int ret;
    if (!msg) {
        return -1;
    }

    int8_t openFlag = 0;
    cJSON *unicastJson = cJSON_Parse(msg);
    if(!unicastJson) {
        return -1;
    }
    LOG_I("App recv msg: [%s]", msg);
    cJSON *params = cJSON_GetObjectItem(unicastJson, "params");
    if (!params) {
        LOG_E("params is null!");
        cJSON_Delete(unicastJson);
        return -1;
    }
	SAMPLE_INFO("runt that ");
    cJSON *boolOpenJson = cJSON_GetObjectItem(params, "detectResult");
    if(boolOpenJson) {
        if (!strcmp("waiting", boolOpenJson->valuestring)) {
          	SAMPLE_INFO("runt that ");

			if(gRunAiSwitch == false) {
				 gRunAiSwitch = true;		
			}	
            LOG_I("latch open!");
        } else {
			if(!strcmp("leave", boolOpenJson->valuestring)){
				send_switch_door(LATCH_STATE_CLOSE);
			}else {
            	LOG_I("recev Error message !!!!! ");
			}
        }
    }
    cJSON_Delete(unicastJson);
	return 0;
}

int runUDP(char *location)
{
	int myInt = 150;
	NetBroadcastPara_t selfDevInfo;
	memset(&selfDevInfo, 0, sizeof(selfDevInfo));
    memcpy(selfDevInfo.name, "3518摄像头", strlen("3518摄像头") + 1);
    memcpy(selfDevInfo.type , "camera", strlen("camera") + 1);
    memcpy(selfDevInfo.id , "860812b65c4881b11e4383520c7307ddad5b890fba2a6c0104fecbe397234126",
    strlen("860812b65c4881b11e4383520c7307ddad5b890fba2a6c0104fecbe397234126") + 1);

	memcpy(selfDevInfo.group , location, strlen(location) + 1);
	SAMPLE_INFO("selfDevInfo.group->%s, gpBuf->%s", selfDevInfo.group, location);
    selfDevInfo.priority = myInt;

    selfDevInfo.publish = CreateDlist();
	selfDevInfo.subscribe = CreateDlist();
    SAMPLE_INFO("run that");
    InsertHdlist(selfDevInfo.subscribe, "carApproachDetect", strlen("carApproachDetect") + 1);
    InsertHdlist(selfDevInfo.publish, "licensePlateRecognition", strlen("licensePlateRecognition") + 1);
    InsertHdlist(selfDevInfo.publish, "latchControl", strlen("latchControl") + 1);
    if(LocalNetSelfInfoSet(&selfDevInfo)) {
        LOG_E("LocalNetSelfInfoSet failed!");
        return -1;
    }

	SAMPLE_INFO("run that");

	LocalNetMsgRecvCbReg(msgRecv);
	int ret = LocalNetInit();
	if (ret < 0) {
		SAMPLE_ERROR("localNetInit");
		return -1;
	}
	return 0;

}

int main(int argc,char **argv)
{
	int ret;
	char sBuf[64] = {0};
	char pBuf[64] = {0};
	char entryBuf[64] = {0};
	char licensePlate[32] = {0};
	char input;
	int mySleep = 2;

	InitCamera();
	sleep(mySleep);

	while(1) {
		Beep("/sdcard/startWpa.mp4");
		sleep(1);
		RunQRCamera();
		sleep(mySleep);
   		ret = getScanSsidPassword(sBuf, pBuf, entryBuf);
	   if(ret >= 0) {
	   	   SAMPLE_INFO("will connect wifi");
		   WriteWpaConfig(sBuf, pBuf);
		   SAMPLE_INFO("the ssid ->%s, pBuf ->%s, uBuf->%s", sBuf, pBuf, entryBuf);
		   WpaClientStart();
		   mySleep = 10;
		   sleep(mySleep);
		   Beep("/sdcard/successWpa.mp4");
		   break;
	   } else {
	   	    Beep("/sdcard/failWpa.mp4");
		    SAMPLE_INFO("getScanSsidPassword is fail");
	   }	
	}
	
	while(cin >> input) {
		switch (input) {
			case '1':
				RunAICamera();
				break;
			case '2':
				ret = http();
				if (ret < 0) {
					SAMPLE_INFO("the http Error");
					continue;
				}
				ret = getValue(licensePlate);
				if( ret > -1) {
					SAMPLE_INFO("licensePlate->%s", licensePlate);
				} else {
					SAMPLE_ERROR("getValue");
				}
				SAMPLE_INFO("ret -> %d, licensePlate->%s", ret, licensePlate);
				break;
			default:
				SAMPLE_ERROR("input Error");
		}
	}
    return 0;
}

int main_(int argc,char **argv)
{
	int ret;
	char sBuf[64] = {0};
	char pBuf[64] = {0};
	char entryBuf[64] = {0};
	char licensePlate[32] = {0};
	int mySleep = 20;
	
	SAMPLE_INFO("run lpr_sample APP");
	sleep(mySleep);

	InitCamera();
	mySleep = 2;
	sleep(mySleep);

	while(1) {
		Beep("/sdcard/startWpa.mp4");
		RunQRCamera();
		sleep(mySleep);
   		ret = getScanSsidPassword(sBuf, pBuf, entryBuf);
	   if(ret >= 0) {
	   	   SAMPLE_INFO("will connect wifi");
		   WriteWpaConfig(sBuf, pBuf);
		   SAMPLE_INFO("the ssid ->%s, pBuf ->%s, uBuf->%s", sBuf, pBuf, entryBuf);
		   WpaClientStart();
		   mySleep = 10;
		   sleep(mySleep);
		   Beep("/sdcard/successWpa.mp4");
		   break;
	   } else {
	   	    Beep("/sdcard/failWpa.mp4");
		    SAMPLE_INFO("getScanSsidPassword is fail");
	   }
		
	}
	
	ret = runUDP(entryBuf);
	if (ret < 0) {
		SAMPLE_INFO("runUDP");
	}
	while(1){
		mySleep = 1;
		sleep(mySleep);
		if(gRunAiSwitch == true) {
			Beep("/sdcard/startLpr.mp4");
#if 1
			RunAICamera();
			ret = http();
			if (ret < 0) {
				SAMPLE_INFO("the http Error");
				continue;
			}

			ret = getValue(licensePlate);
#else
			ret = 0 ;
			strcpy(licensePlate,"湘F:345698");
#endif
			if( ret > -1) {
				SAMPLE_INFO("licensePlate->%s", licensePlate);
				test_send_car_data(licensePlate);
				send_switch_door(LATCH_STATE_OPEN);
				gRunAiSwitch = false;
				Beep("/sdcard/successLpr.mp4");
			} else {
				SAMPLE_ERROR("getValue");
			}
			mySleep = 3;
			sleep(mySleep);
			continue;
		}
	}
    return 0;
}

