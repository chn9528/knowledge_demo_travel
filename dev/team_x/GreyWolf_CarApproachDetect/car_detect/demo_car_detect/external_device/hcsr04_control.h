/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __HSCR04_CONTROL_H__
#define __HSCR04_CONTROL_H__

/**
 * @brief Get the distance,which measure disance by ultrasonic
 * @param none
 * @return distance that type is unsigned  int
 */
unsigned int HCSR04_GetMeasurementDistance(void);

/**
 * @brief init the HCSR04 sensor
 */
void HCSR04_Init(void);

/**
 * @brief deinit the HCSR04 sensor
 */
void HCSR04_Deinit(void);

#endif /* __HSCR04_CONTROL_H__ */
